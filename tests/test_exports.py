"""Meta export (filter/template) tests."""

import pydantic
import pytest
from fw_utils import AttrDict

from fw_meta import ExportRule
from fw_meta.exports import ClassificationFilter

FILE = AttrDict(
    {
        "project.label": "prj",
        "subject.label": "sub",
        "session.label": "ses",
        "acquisition.label": "acq",
        "file.name": "MR1.2.3.dicom.zip",
        "file.type": "dicom",
        "file.tags": ["tag"],
        "file.info.key": "value",
        "file.info.toggle": True,
        "file.classification": {
            "Intent": ["Localizer"],
            "Measurement": ["T1"],
        },
    }
)


def test_export_rule_defaults():
    rule = ExportRule()
    assert rule.level == "acquisition"
    assert rule.include == rule.include == []
    assert rule.path == (
        "{project.label}/{subject.label}/{session.label}/{acquisition.label}/"
        "{file.name}"
    )
    assert rule.unzip
    assert rule.unzip_path == "basename"
    assert rule.match(FILE)
    assert rule.format(FILE) == "prj/sub/ses/acq/MR1.2.3.dicom.zip"


def test_export_rule_filter():
    # info type string
    assert ExportRule(include=["file.info.key=~val"]).match(FILE) is True
    # info type bool
    assert ExportRule(include=["file.info.toggle=true"]).match(FILE) is True
    # alias, explicit intent subkey, case-insensitivity
    assert ExportRule(include=["classification.intent=localizer"]).match(FILE) is True
    # implicit subkey
    assert ExportRule(include=["classification=Localizer"]).match(FILE) is True
    # exclusion
    assert ExportRule(exclude=["classification=Localizer"]).match(FILE) is False
    # include no-match
    assert ExportRule(include=["classification=Functional"]).match(FILE) is False
    # exclude no-match
    assert ExportRule(exclude=["classification=Functional"]).match(FILE) is True
    # include - no classification values
    assert ExportRule(include=["classification=Functional"]).match({}) is False
    # exclude - no classification values
    assert ExportRule(exclude=["classification=Functional"]).match({}) is True


def test_classification_filter_smart_copy_compat():
    smart_copy_item = {"classification": {"Intent": ["Localizer"]}}
    smart_copy_filt = ClassificationFilter("classification.Intent", "=", "Localizer")
    assert smart_copy_filt.match(smart_copy_item)


def test_export_rule_validation():
    with pytest.raises(pydantic.ValidationError, match="invalid field"):
        ExportRule(level="project", include=["session.label=~test"])
    with pytest.raises(pydantic.ValidationError, match="invalid field"):
        ExportRule(level="project", path="{acquisition.label}")
