"""Meta import (extract) tests."""

import os
from datetime import datetime
from functools import partial

import pytest
from fw_utils import AttrDict, ZoneInfo, format_datetime

from fw_meta import ImportRule, MetaData, extract_meta, imports

GI = "group._id"
PL = "project.label"
SL = "subject.label"
SS = "subject.sex"
sL = "session.label"
AL = "acquisition.label"
AT = "acquisition.timestamp"
AZ = "acquisition.timezone"
TZ = os.environ["TZ"] = "Europe/Budapest"
DT = format_datetime(datetime(1999, 12, 30, 13, 37, 59, tzinfo=ZoneInfo(TZ)))
PRJ = "project"
SUB = "subject"
SES = "session"
ACQ = "acquisition"
FILE = AttrDict(
    {
        "path": "sub/ses/acq/file.txt",
        "name": "file.txt",
        "dir": "sub/ses/acq",
        "depth": 4,
        "size": 5,
        "ctime": 1670513195,
        "mtime": 1670513195,
    }
)
DATA = {
    "PatientID": "S012",
    "PatientSex": "M",
    "SeriesDescription": "Brain T2*",
    "SeriesNumber": 2,
    "SeriesDate": "19991230",
    "SeriesTime": "133759",
}


@pytest.mark.parametrize(
    "patient_id,expected",
    [
        ("", {}),
        ("g", {}),  # group._id is min length 2
        ("grp", {GI: "grp"}),
        ("GRP", {GI: "grp"}),  # normalized to lower-case
        ("grp/prj", {GI: "grp", PL: "prj"}),
        ("grp:prj", {GI: "grp", PL: "prj"}),  # alt delimiter
        ("grp^prj", {GI: "grp", PL: "prj"}),  # alt delimiter
        ("sub@grp/prj", {GI: "grp", PL: "prj", SL: "sub"}),
    ],
)
def test_old_routing_string(patient_id, expected):
    data = {"PatientID": patient_id}
    mapping = ("PatientID", r"[{sub}@]{grp}[(/|:|^){prj}]")
    assert extract_meta(data, mappings=[mapping]) == expected


@pytest.mark.parametrize(
    "patient_id,expected",
    [
        ("grp", {GI: "grp"}),
        ("grp/prj", {GI: "grp", PL: "prj"}),
        ("grp/prj/sub", {GI: "grp", PL: "prj", SL: "sub"}),
        ("fw://grp/prj/sub", {GI: "grp", PL: "prj", SL: "sub"}),
    ],
)
def test_new_routing_string(patient_id, expected):
    data = {"PatientID": patient_id}
    mapping = ("PatientID", r"[fw://]{grp}[/{prj}[/{sub}]]")
    assert extract_meta(data, mappings=[mapping]) == expected


@pytest.mark.parametrize(
    "mapping,expected",
    [
        (("PatientID", "{sub}"), {SL: "S012"}),
        (("PatientID", "{sub:\\d*}"), {}),
        (("PatientID", "{sub:s*}"), {}),
        (("PatientID", "{sub:s*}!i"), {SL: "S012"}),
        (("PatientID", "{sub:s\\d+}!i"), {SL: "S012"}),
        (("PatientID", "S{sub}"), {SL: "012"}),
        (("PatientSex", "{sex}"), {SS: "male"}),
        (("{SeriesNumber} {SeriesDescription}", "{acq}"), {AL: "2 Brain T2star"}),
        (("{SeriesDate} {SeriesTime}", "{acq.time}"), {AT: DT, AZ: TZ}),
    ],
)
def test_extract_mappings(mapping, expected):
    assert extract_meta(DATA, mappings=[mapping]) == expected


def test_extract_mappings_meta():
    mappings = [
        ("path", "{sub}/{ses}/{acq}/*"),
        ("{!sub}-{!ses}-{!acq}.txt", "file"),
    ]
    assert extract_meta(FILE, mappings=mappings) == {
        "subject.label": "sub",
        "session.label": "ses",
        "acquisition.label": "acq",
        "file.name": "sub-ses-acq.txt",
    }


def test_extract_defaults():
    extract = partial(extract_meta, mappings=[("SomeTag", "grp")])
    assert extract({"SomeTag": "foo"}, defaults={"grp": "bar"}) == {GI: "foo"}
    assert extract({}, defaults={"grp": "bar"}) == {GI: "bar"}


def test_extract_overrides():
    extract = partial(extract_meta, mappings=[("SomeTag", "grp")])
    assert extract({"SomeTag": "foo"}, overrides={"grp": "bar"}) == {GI: "bar"}
    assert extract({}, overrides={"grp": "bar"}) == {GI: "bar"}


def test_metadata_class():
    meta = MetaData({SL: "sub", PL: "prj", GI: "grp"})
    # key validation
    with pytest.raises(ValueError, match="invalid field"):
        meta["foo"] = "bar"
    # key alias resolution (setdefault / setitem / contains / getitem)
    assert meta.setdefault("ses", "foo") == "foo"
    assert "ses" in meta
    assert meta["ses"] == meta["session.label"] == "foo"
    meta["ses"] = "ses"
    assert meta["ses"] == meta["session.label"] == "ses"
    # empty value guard
    assert meta.setdefault("acq", "") is None
    assert "acq" not in meta
    # sort order
    assert meta.keys() == list(meta) == [GI, PL, SL, sL]
    assert meta.values() == ["grp", "prj", "sub", "ses"]
    assert list(meta.items()) == [(GI, "grp"), (PL, "prj"), (SL, "sub"), (sL, "ses")]
    # attr access
    assert meta.group == {"_id": "grp"}
    assert meta.group._id == "grp"
    # json support
    assert meta.json == (
        b'{"group":{"_id":"grp"},'
        b'"project":{"label":"prj"},'
        b'"subject":{"label":"sub"},'
        b'"session":{"label":"ses"}}'
    )


def test_load_cont_id():
    assert imports.load_cont_id("0" * 24) == "0" * 24
    assert imports.load_cont_id("foo") is None


def test_load_file_name():
    assert imports.load_file_name("*:" * 33) == "star_" * 33


def test_load_subj_sex():
    assert imports.load_subj_sex("foo") is None


def test_load_sess_age():
    assert imports.load_sess_age("13") == 13
    assert imports.load_sess_age(13) == 13
    assert imports.load_sess_age(13.0) == 13
    assert imports.load_sess_age("foo") is None


def test_load_sess_weight():
    assert imports.load_sess_weight("13") == 13.0
    assert imports.load_sess_weight(13) == 13.0
    assert imports.load_sess_weight(13.0) == 13.0
    assert imports.load_sess_weight("foo") is None


def test_load_tags():
    assert not imports.load_tags("")
    assert imports.load_tags("x") == ["x"]
    assert imports.load_tags("x,y") == ["x", "y"]


def test_default_meta():
    data = AttrDict(get_meta=lambda: {AL: "1.2.3"})
    assert extract_meta(data) == {AL: "1.2.3"}


def test_import_rule_defaults():
    rule = ImportRule(mappings="name={name}")
    assert rule.include == rule.include == []
    assert rule.match(FILE)
    assert rule.extract(FILE) == {
        "acquisition.label": "acq",
        "file.name": "file.txt",
        "session.label": "ses",
        "subject.label": "sub",
    }


def test_import_rule_filter():
    Rule = partial(ImportRule, mappings="path={sub}/{ses}/{acq}/{file}")
    assert Rule(include=["size>4"]).match(FILE) is True
    assert Rule(include=["size>5"]).match(FILE) is False
    assert Rule(exclude=["created>2020"]).match(FILE) is False
    assert Rule(exclude=["created<2020"]).match(FILE) is True


def test_import_rule_dst_fields():
    rule = ImportRule(
        mappings="path={sub}/{file}", defaults=["ses=foo"], overrides=["acq=bar"]
    )
    expected = {"subject.label", "file.name"}
    # NOTE defaults / overrides not included since hidden
    assert rule.dst_fields == expected


def test_import_rule_pattern_does_not_match_forward_slash():
    rule = ImportRule(mappings="path={sub}")
    assert "subject.label" not in rule.extract({"path": "foo/bar"})
    rule = ImportRule(mappings="path={sub}/{file}")
    assert "subject.label" not in rule.extract({"path": "foo/bar/file.txt"})


def test_import_rule_type():
    rule = ImportRule(mappings="path={acq}/*.dcm", type="dicom")
    assert rule.type == "dicom"
    assert rule.group_by == "dir"
    assert rule.zip is None
    data = {"path": "acq/slice.dcm", "dir": "acq"}
    assert rule.group(data) == "acq"
    assert rule.extract(data) == {
        "acquisition.label": "acq",
        "file.type": "dicom",
    }


@pytest.mark.parametrize(
    "level,data,expected",
    [
        (PRJ, {"path": "file"}, {}),
        (SUB, {"path": "sub/file"}, {SL: "sub"}),
        (SES, {"path": "ses/file"}, {sL: "ses"}),
        (SES, {"path": "sub/ses/file"}, {SL: "sub", sL: "ses"}),
        (ACQ, {"path": "acq/file"}, {AL: "acq"}),
        (ACQ, {"path": "ses/acq/file"}, {sL: "ses", AL: "acq"}),
        (ACQ, {"path": "sub/ses/acq/file"}, {SL: "sub", sL: "ses", AL: "acq"}),
    ],
)
def test_import_rule_level_defaults(level, data, expected):
    rule = ImportRule(level=level)
    assert rule.match(data)
    assert rule.extract(data) == expected
